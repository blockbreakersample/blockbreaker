﻿using UnityEngine;
using System.Collections;
/// <summary>
/// ボスが使う拡散弾のスクリプト
/// </summary>
public class DiffusionBullet : MonoBehaviour
{
    //拡散する範囲は必ず0~1の値までにする
    [Range(0f, 1f)]
    //拡散する角度を格納する変数の宣言
    [SerializeField]
    private float diffusionAngle;
    //弾のスピードを格納する変数の宣言
    [SerializeField]
    private float bulletSpeed;
    //プレイヤーの情報を格納する変数
    GameObject player;
    //弾を前方(オブジェクトが向いている方向)に進ませる変数
    private Vector3 forward;
    //物理演算の定義
    private Rigidbody rigid;

    private void Start()
    {
        //プレイヤーを取得
        player = GameObject.Find("Player");
        //物理演算を取得
        rigid = this.GetComponent<Rigidbody>();

        //生成される弾の一つ一つにランダムにx軸、y軸、z軸の値を設定して
        //弾が発射される方向を不規則にする
        //ただし今回は前後への拡散は行わない
        float angle_x = Random.Range(-diffusionAngle, diffusionAngle);
        float angle_y = Random.Range(-diffusionAngle, diffusionAngle);
        float angle_z = Random.Range(-diffusionAngle, diffusionAngle);

        //オブジェクトが向いている方向に進む弾にランダムに角度を入れて、色んな方向に飛んでいくようにする
        forward = player.transform.forward + new Vector3(angle_x, angle_y, angle_z);
        //親オブジェクトについていかないようにする(親オブジェクトを解除する)
        this.transform.parent = null;
    }

    private void Update()
    {
        //弾を下に動かすコード
        //パソコンの処理速度がコードに影響しないようにTime.deltaTimeで処理速度を一定にする。
        //この時弾の動く方向を下方向に限定しているため、弾が上に行くことはない(遅くはなる)
        transform.position += new Vector3(0, -bulletSpeed * Time.deltaTime, 0);
        {
            //弾が正面を向いている方向に弾のスピードを代入し、その方向(拡散する方向)に進ませる
            rigid.velocity = forward * bulletSpeed;
        }
    }
    //Colliderが他のトリガーイベントに侵入したときに呼び出される関数(物理的な干渉はしない)
    protected void OnTriggerEnter(Collider other)
    {
        //衝突したオブジェクトのtagがsidewall(壁)ならば
        if (other.gameObject.tag == "sidewall")
        {
            //forward.x(x軸の方向に進む速度)に－の値を乗算(代入演算)することでx軸の進む速度、方向を反転させる
            forward.x *= -1;
        }

            //bottomwallのタグのついたトリガーに触った時
            if (other.gameObject.tag == "bottomwall")
            {
            //オブジェクトを消滅させる
                Destroy(this.gameObject);
            }
            //Blockのタグのついたトリガーに触った時
            if (other.gameObject.tag == "Block")
            {
            //オブジェクトを消滅させる
                Destroy(this.gameObject);
            }
        }
    }