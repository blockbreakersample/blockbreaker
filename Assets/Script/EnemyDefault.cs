﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

/// <summary>
/// 敵の普遍的な行動を継承させるクラス
/// </summary>
public class EnemyDefault : MonoBehaviour
{
    // 最初の敵を生成する場所(Inspecterで指定する)
    [SerializeField]
    protected Vector2 createPosition;

    // 移動スピード(Inspecterで指定する)
    [SerializeField]
    protected float speed;

    // 敵のX軸の移動方向（初期値は左へ移動）
    [SerializeField]
    protected int directionX = -1;

    // Use this for initialization
    protected virtual void Start()
    {

    }

    // Update is called once per frame
    //virtualキーワードでoverride(上書き)が出来るようにする
    protected virtual void Update()
    {

    }
    /// <summary>
    /// x軸・Y軸の方向にオブジェクト(敵)を動かすためのコード
    /// </summary>
    /// <param name="direction"></param>
    ///virtualキーワードでoverride(上書き)が出来る様にする
    protected virtual void Move(Vector2 direction)
    {

        //敵の現在の座標を取得
        Vector2 position = transform.position;

        // 移動量を加える(Time.deltaTimeで速度を一定にする)
        position += direction * speed * Time.deltaTime;

        // 制限をかけた値を新しい位置とする
        transform.position = position;
    }
}


