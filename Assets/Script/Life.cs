﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/// <summary>
/// 残機に関するスクリプト
/// </summary>
public class Life : MonoBehaviour
{
    //最初の残機の数を設定する
    //staticで規定された値はゲーム全体で保持されるため
    //オブジェクトが破棄されても残り続ける。
    //これによってシーンで残機数を共有する。
    private static int life = 3;

    //ボールオブジェクトを格納する変数を宣言
    private GameObject Ball;
    //プレイヤーオブジェクトを格納する変数の宣言
    [SerializeField]
    private GameObject player;
    //敵オブジェクトを格納する変数の宣言
    [SerializeField]
    private GameObject Enemy;
    //UFOオブジェクトを格納する変数の宣言
    [SerializeField]
    private GameObject UFO;
    //シーン遷移をするためのシーンオブジェクトを格納する変数の宣言
    [SerializeField]
    private GameObject scene;
    //ゲームオーバー画面を表示するためのオブジェクトを格納するための変数の宣言
    [SerializeField]
    private GameObject gameoverObject;

    void Start()
    {
        //プレイヤーオブジェクトを取得する
        player = GameObject.Find("Player");
        //ボールのオブジェクトを取得する
        Ball = GameObject.Find("Ball");
        //敵のオブジェクトを取得する
        Enemy = GameObject.Find("EnemyObject");
        UFO = GameObject.Find("UFOObject");
        //シーン遷移のオブジェクトを取得する
        scene = GameObject.Find("SceneObject");

    }

    void Update()
    {
        //残機を表すTextのコンポーネントを取得して
        //スクリプトによって残機数を書き換えられるようにする
        GetComponent<Text>().text = "残機:" + life;

        //プレイヤーが見つからず(敵にやられた、ボールを下に落とした)、
        //残機が一つ以上ある場合は(残機が現在の数よりも1つ少ない数以上であれば)
        if (player == null && life >= life - 1)
        {
            //残機を1つ減らす(同じ数にする)
            life--;
            //ゲームオーバー画面には切り替えず
            gameoverObject.SetActive(false);
            //再び最初に戻る(ゲームが開始される)
            SceneManager.LoadScene("Main");
        }
        //残機がなくなったら(残機の数が0になったら)
        else if (life == 0)
        {
            //敵を削除して
            Destroy(Enemy.gameObject);
            Destroy(UFO.gameObject);
            //ボールオブジェクトを削除して
            Destroy(Ball.gameObject);
            //残機とスコアを初期化して
            life = 3;
            Score.score = 0;
 
            //その後、ゲームオーバー画面に切り替える
            gameoverObject.SetActive(true);
        }
    }
}