﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
/// <summary>
///プレイヤー(自機)の移動に関するスクリプト
/// </summary>
public class Player : MonoBehaviour
{
    /// <summary>２Ðのマウス位置座標(x,y,z)を定義する変数(初期値は０)</summary>
    private Vector3 mousePointposition;

    /// <summary>3Dのマウス位置座標(x,y,z)を定義する変数(初期値は０)</summary>
    private Vector3 worldMousePointPosition;

    /// <summary>プレイヤーの正の値の移動制限を定義する定数</summary>
    private const float MAX_RIGHT = 2.3f;

    /// <summary>プレイヤーの負の値の移動制限を定義する定数</summary>
    private const float MIN_LEFT = -2.3f;

    /// <summary>プレイヤーのy軸を固定する定数</summary>
    private const float MAX_UP = -4.0f;
    //ボールオブジェクトを格納する変数を宣言
    GameObject Ball;
    //爆発オブジェクトを格納する変数を宣言
    public GameObject explosion;
    //ゲームオーバーテキストを格納する変数を宣言
    public GameObject gameOverObject;

    // Use this for initialization
    void Start()
    {
        //ボールのオブジェクトを取得する
        Ball = GameObject.Find("Ball");

    }

    // Update is called once per frame
    private void Update()
    {

        //マウス位置座標(スクリーン座標、スマホならばタップした位置)を２DのVector3で取得
        mousePointposition = Input.mousePosition;
        //マウス位置座標をスクリーン座標(２Ð)からワールド座標(３Ð)に変換する
        worldMousePointPosition = Camera.main.ScreenToWorldPoint(mousePointposition);

        //壁(カメラ表示)を突き抜けないように自機の動きを制限する
        //X(横)軸のマウス位置座標が左に定数MIN_LEFT以下しか動いていなければ
        if (worldMousePointPosition.x <= MIN_LEFT)
        {
            //マウス位置座標は左に定数MIN_LEFTの所まで取得できる。(プレイヤーは左に2.1fまで動くことができる。)
            worldMousePointPosition.x = MIN_LEFT;
        }
        //X(横)軸のマウス位置座標が右に定数MAX_RIGHT以上動いているのであれば
        else if (worldMousePointPosition.x >= MAX_RIGHT)
        {
            //マウス位置座標は右に定数MAX_RIGHTまでしか取得されない。(プレイヤーは右に2.1fまで動くことができる。)
            worldMousePointPosition.x = MAX_RIGHT;
        }

        //y軸とz軸は動かす必要がないため、変数によって固定する。
        worldMousePointPosition.y = MAX_UP;
        worldMousePointPosition.z = 0.0f;

        //ワールド座標(マウス位置座標)をPlayer位置へ代入する(マウスの位置にプレイヤーが来るようにする)
        gameObject.transform.position = worldMousePointPosition;

    }

    //Colliderが他のトリガーイベントに侵入したときに呼び出される関数(物理的な干渉はしない)
    void OnTriggerEnter(Collider other)
    {
        //敵の弾がプレイヤーに触れたならば
        if (other.gameObject.tag == "EnemyBullet")
        {
            //プレイヤーの位置に爆発を生成して
            Instantiate(explosion, transform.position, transform.rotation);
            //ボールオブジェクトを削除して
            Destroy(Ball.gameObject);
            //その後、ゲームオーバー画面に切り替える
            gameOverObject.SetActive(true);
            //最後にプレイヤー(自機)を消滅させる
            Destroy(this.gameObject);
        }
        ///Unityの機能でオブジェクトのtagを設定しておく
        //触れたオブジェクトのtagがEnemyならば
        if (other.gameObject.tag == "Enemy")
        {
            //プレイヤーの位置に爆発を生成して
            Instantiate(explosion, transform.position, transform.rotation);
            //ボールオブジェクトを削除して
            Destroy(Ball.gameObject);
            //その後、ゲームオーバー画面に切り替える
            gameOverObject.SetActive(true);
            //最後にプレイヤー(自機)を消滅させる
            Destroy(this.gameObject);
        }
    }
}
