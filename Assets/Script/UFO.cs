﻿using UnityEngine;
using System.Collections;

/// <summary>
/// UFO を誕生させるスクリプト
/// </summary>
public class UFO : MonoBehaviour {
    //UFOプレハブをinspecterで格納する
    [SerializeField]
    private GameObject UFOPrefab;
    // 待ち時間
    private float waitTime;
    // 待ち時間計測.
    private float waitTimer = 0.0f;
    //ランダム数値の最低値
    private float range_x = 20.0f;
    //ランダム数値の最大値
    private float range_y = 50.0f;

    // Use this for initialization
    void Start() {
        //待機時間の初期化
        waitTime = Random.Range(range_x, range_y);
    }

    // Update is called once per frame
    void Update()
    {
        // 時間を測る(時間を加算していく)
        waitTimer += Time.deltaTime;
        // 待ち時間が経過したら
        if (waitTimer > waitTime)
        {
            // 次の待ち時間をランダムに設定
            waitTime = Random.Range(range_x, range_y);
            //UFOObjectの位置にUFOを生成
            Instantiate(UFOPrefab, transform.position, transform.rotation);
            // タイマーは0に戻す
            waitTimer = 0.0f;

        }
    }
}
