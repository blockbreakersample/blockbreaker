﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
/// <summary>
/// ボールの動きを規定するスプリクト
/// </summary>
public class Ball : MonoBehaviour
{

    //AudioSourceを格納する変数の宣言
    //一つ目は壁に反射する際の音(ここではre_wallとする)
    private AudioSource re_wall;
    //二つ目は敵を倒すときの音(ここではEnemyAtとする)
    private AudioSource EnemyAt;

    /// <summary>ボールのx軸の速度を定義する変数(初期値０)</summary>
    public static float ball_x;
    /// <summary>ボールのy軸の速度を定義する変数(初期値０)</summary>
    public static float ball_y;

    //プレイヤーオブジェクトを格納する変数を宣言
    GameObject player;
    //爆発オブジェクトを格納する変数を宣言
    [SerializeField]
    private GameObject explosion;
    //ゲームオーバーテキストを格納する変数を宣言
    [SerializeField]
    private GameObject gameOverObject;

    // Use this for initialization
    void Start()
    {
        //AudioSourceのコンポーネントを取得(二つあるのでそれぞれを取得できるよう配列で取得)
        AudioSource[] audioSources = GetComponents<AudioSource>();
        re_wall = audioSources[0];
        EnemyAt = audioSources[1];
        //プレイヤーオブジェクトを取得
        player = GameObject.Find("Player");
        //ボールのx軸の速度の初期値を2.0Fに設定
        ball_x = 3.5f;
        //ボールのy軸の速度の初期値を2.0Fに設定
        ball_y = 3.5f;

    }

    // Update is called once per frame
    void Update()
    {
        //ボールを右斜め上に動かしていくコード
        //パソコンの処理速度がコードに影響しないようにTime.deltaTimeで処理速度を一定にする。
        transform.position += new Vector3(ball_x * Time.deltaTime, ball_y * Time.deltaTime, 0);

    }
    /// <summary>Rigidbody(物理演算)を持つオブジェクトとColliderを持つオブジェクトが衝突した時にこの関数が呼ばれる</summary>
    /// <param name="collision"></param>
    void OnCollisionEnter(Collision collision)
    {

        ///Unityの機能でオブジェクトのtagを設定しておく
        //衝突したオブジェクトのtagがsidewall_right(右の壁)ならば
        if (collision.gameObject.tag == "sidewall")
        {
            //壁に反射する音を鳴らす(PlayOneShotで一回だけ鳴らす)
            re_wall.PlayOneShot(re_wall.clip);
            //ball_x(x軸の方向に進む速度)に－の値を乗算(代入演算)することでx軸の進む速度、方向を反転させる
            ball_x *= -1;

        }
        //衝突したオブジェクトのtagがtopwall(上の壁)ならば
        if (collision.gameObject.tag == "topwall")
        {
            //壁に反射する音を鳴らす(PlayOneShotで一回だけ鳴らす)
            re_wall.PlayOneShot(re_wall.clip);

            //ball_y(y軸の方向に進む速度)に－の値を乗算(代入演算)することでy軸の進む速度、方向を反転させる
            ball_y *= -1;
 
        }
        //衝突したオブジェクトのtagがbottomwall(下の壁)ならば
        if (collision.gameObject.tag == "bottomwall")
        {
            //プレイヤーの位置に爆発を生成して
            Instantiate(explosion, transform.position, transform.rotation);
            //ボールオブジェクトを削除して
            Destroy(player.gameObject);
            //その後、ゲームオーバー画面に切り替える
            gameOverObject.SetActive(true);
            //その後ボールを消滅させる
            Destroy(this.gameObject);
        }

        //switch文
        switch (collision.gameObject.tag)
        {
            //caseで分ける
            //PlaterTopのタグがついているColliderにぶつかったら
            case "PlayerTop":
                //壁に反射する音を鳴らす(PlayOneShotで一回だけ鳴らす)
                re_wall.PlayOneShot(re_wall.clip);
                //ball_y(y軸の方向に進む速度)に－の値を乗算(代入演算)することでy軸の進む速度、方向を反転させる(角度を少し変える)
                if (ball_y <= -1)
                {
                    ball_y *= -1.5f;
                }
                //角度が初期値よりも大きくなろうとしても
                if (ball_y > 2)
                {
                    //y軸の角度は初期値を超えない様にする
                    ball_y = 2;
                }

                break;
            //PlayerBottomのタグが付いているColliderにぶつかったら
            case "PlayerBottom":
                //壁に反射する音を鳴らす(PlayOneShotで一回だけ鳴らす)
                re_wall.PlayOneShot(re_wall.clip);
                //ball_y(y軸の方向に進む速度)に－の値を乗算(代入演算)することでy軸の進む速度、方向を反転させる
                if (ball_y >= 1)
                {
                    ball_y *= -1.0f;

                }
                //角度が初期値よりも大きくなろうとしても
                if (ball_y > 2)
                {
                    //y軸の角度は初期値を超えない様にする
                    ball_y = 2;
                }
                break;
            //PlaterTopleftのタグがついているColliderにぶつかったら
            case "PlayerTopleft":
                //壁に反射する音を鳴らす(PlayOneShotで一回だけ鳴らす)
                re_wall.PlayOneShot(re_wall.clip);
                //ボールがx軸の方向に+の値を持っていたら(右方向にボールが動いてたら)
                //かつプレイヤーがy軸にーの値を持っていたら(下方向に動いていたら)
                if (ball_x > 0 && ball_y <= -1)
                {
                    //ball_x(x軸の進む方向)に－の値を乗算(代入演算)することでx軸の進む速度、方向を反転させる
                    Debug.Log("bbb");
                    ball_x *= -1;

                }

                else if (ball_x > 0 && ball_y >= 1)
                {
                    //ball_x(x軸の進む方向)はそのまま
                    Debug.Log("ccc");
                    //ball_y(y軸の方向に進む速度)に－の値を乗算(代入演算)することでy軸の進む速度、方向を反転させる(左に角度が付くように)
                    ball_y *= -0.5f;
                }
                //ただしball_y(y軸)は初期値の半分よりも小さい値にならないようにする
                //初期値の半分の値を下回る値なろうとしたら
                if (ball_y < 1)
                {
                    //y軸の値は１で固定する
                    ball_y = 1;
                }
                break;

            //PlaterToprightのタグがついているColliderにぶつかったら
            case "PlayerTopright":
                //壁に反射する音を鳴らす(PlayOneShotで一回だけ鳴らす)
                re_wall.PlayOneShot(re_wall.clip);
                //ボールがx軸の方向に+の値を持っていたら(右方向にボールが動いていたら)
                //かつプレイヤーがy軸にーの値を持っていたら(下方向に動いていたら)
                if (ball_x > 0 && ball_y <= -1)
                {
                    //ball_x(x軸の進む方向)はそのまま
                    Debug.Log("ddd");
                    //ball_y(y軸の方向に進む速度)に－の値を乗算(代入演算)することでy軸の進む速度、方向を反転させる(右に角度が付くように)
                    ball_y *= -0.5f;
                }
                else
                //ボールがx軸の方向に-の値を持っていたら(左方向にボールが動いていたら)
                {
                    //ball_x(x軸の進む方向)+の値を乗算(代入演算)することでx軸の進む速度、方向を反転させる
                    Debug.Log("eee");
                    ball_x *= -1;
                    //ball_y(y軸の方向に進む速度)に－の値を乗算(代入演算)することでy軸の進む速度、方向を反転させる(右に角度が付くように)
                    ball_y *= -0.5f;
                }
                //ただしball_y(y軸)は初期値の半分よりも小さい値にならないようにする
                //初期値の半分の値を下回る値なろうとしたら
                if (ball_y < 1)
                {
                    //y軸の値は１で固定する
                    ball_y = 1;
                }
                break;

            //PlaterTopleftのタグがついているColliderにぶつかったら
            case "PlayerBottomleft":
                //壁に反射する音を鳴らす(PlayOneShotで一回だけ鳴らす)
                re_wall.PlayOneShot(re_wall.clip);
                //ボールがx軸の方向に+の値を持っていたら(右方向にボールが動いてたら)
                //かつプレイヤーがy軸に＋の値を持っていたら(上方向に動いていたら)
                if (ball_x > 0 && ball_y >= 1)
                {
                    //ball_x(x軸の進む方向)に－の値を乗算(代入演算)することでx軸の進む速度、方向を反転させる
                    ball_x *= -1;
                    //ball_y(y軸の方向に進む速度)は角度を付けるが方向は変わらない
                    ball_y *= 0.5f;

                }
                else

                //ボールがx軸の方向に-の値を持っていたら(左方向にボールが動いていたら)
                {
                    //ball_x(x軸の進む方向)はそのまま
                    //ball_y(y軸の方向に進む速度)は角度を付けるが方向は変わらない
                    ball_y *= 0.5f;
                }
                //ただしball_y(y軸)は初期値の半分よりも小さい値にならないようにする
                //初期値の半分の値を下回る値なろうとしたら
                if (ball_y < 1)
                {
                    //y軸の値は１で固定する
                    ball_y = 1;
                }
                break;

            //PlaterToprightのタグがついているColliderにぶつかったら
            case "PlayerBottomright":
                //壁に反射する音を鳴らす(PlayOneShotで一回だけ鳴らす)
                re_wall.PlayOneShot(re_wall.clip);
                //ボールがx軸の方向に+の値を持っていたら(右方向にボールが動いていたら)
                //かつプレイヤーがy軸に＋の値を持っていたら(上方向に動いていたら)
                if (ball_x > 0 && ball_y >= 1)
                {
                    //ball_x(x軸の進む方向)はそのまま

                    //ball_y(y軸の方向に進む速度)は角度を付けるが方向は変わらない
                    ball_y *= 0.5f;
                }
                else
                //ボールがx軸の方向に-の値を持っていたら(左方向にボールが動いていたら)
                {
                    //ball_x(x軸の進む方向)+の値を乗算(代入演算)することでx軸の進む速度、方向を反転させる
                    ball_x *= 1;
                    //ball_y(y軸の方向に進む速度)は角度を付けるが方向は変わらない
                    ball_y *= 0.5f;
                }
                //ただしball_y(y軸)は初期値の半分よりも小さい値にならないようにする
                //初期値の半分の値を下回ろうとしたら
                if (ball_y < 1)
                {
                    //y軸の値は１で固定する
                    ball_y = 1;
                }
                break;

            //UFOのタグが付いているオブジェクトにぶつかった時
            case "UFO":
                //壁に反射する音を鳴らす(PlayOneShotで一回だけ鳴らす)
                EnemyAt.PlayOneShot(EnemyAt.clip);
                //ボールが右上に向かって進んでいれば
                if(ball_x >= 1 && ball_y >= 1)
                {
                    //y軸を反転させて右下に進ませる
                    ball_y *= -1;
                }
                //ボールが右下に向かって進んでいれば
                else if(ball_x >= 1 && ball_y <= -1)
                {
                    //x軸を反転させて左下に進ませる
                    ball_x *= -1;
                }
                //ボールが左上に進んでいれば
                else if (ball_x <= -1 && ball_y >= 1)
                {
                    //y軸を反転させて左下に進ませる
                    ball_y *= -1;
                }
                //ボールが左下に進んでいれば
                else
                {
                    //x軸を反転させて右下に進ませる
                    ball_x *= -1;
                }
                //角度が初期値よりも大きくなろうとしても
                if (ball_y > 2)
                {
                    //y軸の角度は初期値を超えない様にする
                    ball_y = 2;
                }
                break;

            default:
                break;
        }
    }
    ///ボールが他のオブジェクトのコライダーに触れた場合に処理されるコード
    void OnTriggerEnter(Collider other)
    {
        //EnemyTopのタグを持つコライダーに触れた場合
        if (other.gameObject.tag == "EnemyTop")
        {
            //敵と衝突する音を鳴らす(PlayOneShotで一回だけ鳴らす)
            EnemyAt.PlayOneShot(EnemyAt.clip);
            //y軸(上下)を反転させる
            ball_y *= -1;

        }
        //EnemyLeftのタグを持つコライダーに触れた場合
        if (other.gameObject.tag == "EnemyLeft")
        {
            //敵と衝突する音を鳴らす(PlayOneShotで一回だけ鳴らす)
            EnemyAt.PlayOneShot(EnemyAt.clip);
            //x軸(左右)を反転させる
            ball_x *= -1;
        }
        //EnemyRightのタグを持つコライダーに触れた場合
        if (other.gameObject.tag == "EnemyRight")
        {
            //敵と衝突する音を鳴らす(PlayOneShotで一回だけ鳴らす)
            EnemyAt.PlayOneShot(EnemyAt.clip);
            //x軸(左右)を反転させる
            ball_x *= -1;
        }
        //EnemyBottomのタグを持つコライダーに触れた場合
        if (other.gameObject.tag == "EnemyBottom")
        {
            //敵と衝突する音を鳴らす(PlayOneShotで一回だけ鳴らす)
            EnemyAt.PlayOneShot(EnemyAt.clip);
            //y軸(上下)を反転させる
            ball_y *= -1;
        }
    }
}