﻿using UnityEngine;
using System.Collections;

public class Block : MonoBehaviour {

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    //Colliderが他のトリガーイベントに侵入したときに呼び出される関数(物理的な干渉はしない)
    void OnTriggerEnter(Collider other)
    {
        //Enemyのタグのついたトリガーに触った時
        if (other.gameObject.tag == "Enemy")
        {
            Destroy(this.gameObject);
        }
        //EnemyBulletのタグのついたトリガーに触った時
        if (other.gameObject.tag == "EnemyBullet")
        {
            Destroy(this.gameObject);
        }
    }
}
