﻿using UnityEngine;
using System.Collections;

public class BulletMove : MonoBehaviour
{
    //弾が飛んでいくスピード
    [SerializeField]
    protected float Speed;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    protected virtual void Update()
    {
        //弾を下に動かしていくコード
        //パソコンの処理速度がコードに影響しないようにTime.deltaTimeで処理速度を一定にする。
        transform.position += new Vector3(0, -Speed * Time.deltaTime, 0);
    }
    //Colliderが他のトリガーイベントに侵入したときに呼び出される関数(物理的な干渉はしない)
    protected virtual void OnTriggerEnter(Collider other)
    {
        //bottomwallのタグのついたトリガーに触った時
        if (other.gameObject.tag == "bottomwall")
        {
            Destroy(this.gameObject);
        }
        //Blockのタグのついたトリガーに触った時
        if (other.gameObject.tag == "Block")
        {
            Destroy(this.gameObject);
        }
    }
 }
 