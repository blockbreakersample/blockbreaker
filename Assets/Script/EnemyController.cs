﻿using UnityEngine;
using System.Collections;

public class EnemyController : MonoBehaviour
{

    // この位置に敵が来たらブロックを破壊
    private static float DESTROY_BLOCK_POSITION_Y = -2f;
    // 下に移動する距離
    private static float MOVE_DOWN = 0.25f;

    // 移動スピード
    public float speed;

    // 弾を撃つ間隔
    public float shotDelay;

    //　敵の弾（強制ゲームオーバーに使用）
    public GameObject enemybullet;

    // Waveの一番左端の位置
    Vector2 leftWaveMax;
    // Waveの一番右端の位置
    Vector2 rightWaveMax;
    // Waveの一番下の位置
    Vector2 downWaveMax;

    // Waveの移動方向（初期値は左へ移動）
    int directionX = -1;

    // Player	
    GameObject player;

    // Use this for initialization
    void Start()
    {
        player = GameObject.Find("Player");
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.childCount > 0)
        {
            GetWaveMaxPos();
            Move(transform.right * directionX);
        }
    }

    void Move(Vector3 direction)
    {

        // Waveの座標を取得
        Vector3 pos = transform.position;

        // 移動量を加える
        pos += direction * speed * Time.deltaTime;

        // 最大値を制限（暴走して下に突っ込む可能性の対策）
        pos.x = Mathf.Clamp(pos.x, leftWaveMax.x, rightWaveMax.x);

        // 左右の端に到達している時は少し下へ移動
        if (pos.x <= leftWaveMax.x || pos.x >= rightWaveMax.x)
        {
            directionX = directionX * -1;
            pos.y = pos.y - MOVE_DOWN;
            // ブロックを破壊する位置まで敵が来たらブロックを破壊
            if (pos.y <= DESTROY_BLOCK_POSITION_Y)
            {
                GameObject[] Blocks = GameObject.FindGameObjectsWithTag("Block");
                foreach (var b in Blocks)
                {
                    Destroy(b.gameObject);
                }
            }
        }
        // プレイヤーを超えた時はゲームオーバー(プレイヤーの位置に弾を出現させる)
        if (pos.y <= downWaveMax.y)
        {
            Instantiate(enemybullet,
                         player.transform.position,
                         enemybullet.transform.rotation);
        }

        // 制限をかけた値を新しい位置とする
        transform.position = pos;
    }

    // 敵キャラの中で一番右端と一番左端と一番下の位置を格納
    void GetWaveMaxPos()
    {

        int i;
        Transform tmpTran;

        // 画面左下のワールド座標をビューポートから取得
        Vector2 min = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));

        // 画面右上のワールド座標をビューポートから取得
        Vector2 max = Camera.main.ViewportToWorldPoint(new Vector2(1, 1));

        // 敵キャラの中で一番左端の位置
        Vector2 leftMax = new Vector2(999f, 0f);
        // 敵キャラの中で一番右端の位置
        Vector2 rightMax = new Vector2(-999f, 0f);
        // 敵キャラの中で一番下の位置
        Vector2 downMax = new Vector2(0f, 999f);

        // 全ての敵キャラの位置を調べる
        for (i = 0; i < transform.childCount; i++)
        {
            tmpTran = transform.GetChild(i);

            // 左端
            if (tmpTran.position.x < leftMax.x)
            {
                leftMax = tmpTran.position;
            }
            // 右端
            if (tmpTran.position.x > rightMax.x)
            {
                rightMax = tmpTran.position;
            }
            // 下端
            if (tmpTran.position.y < downMax.y)
            {
                downMax = tmpTran.position;
            }

        }

        // Waveの最大位置を計算
        leftWaveMax.x = min.x - leftMax.x + 0.25f + transform.position.x;
        rightWaveMax.x = max.x - rightMax.x - 0.25f + transform.position.x;
        downWaveMax.y = player.transform.position.y - downMax.y + 0.25f + transform.position.y;
    }

}
