﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/// <summary>
/// 得点の加算に関するスクリプト
/// </summary>
public class Score : MonoBehaviour {

    // スコア
    public static int score;

    //ボールオブジェクトを格納する変数を宣言
    private GameObject Ball;
    //プレイヤーオブジェクトを格納する変数の宣言
    [SerializeField]
    private GameObject player;
    //ゲームクリア時にテキストを表示させるオブジェクトを格納する変数の宣言
    [SerializeField]
    private GameObject GameClearObject;
    //敵オブジェクトを格納する変数の宣言
    [SerializeField]
    private GameObject Enemy;
    //UFOオブジェクトを格納する変数の宣言
    [SerializeField]
    private GameObject UFO;
    //シーン遷移をするためのシーンオブジェクトを格納する変数の宣言
    [SerializeField]
    private GameObject scene;

    void Start()
    {
        //ボールのオブジェクトを取得する
        Ball = GameObject.Find("Ball");
        //敵のオブジェクトを取得する
        Enemy = GameObject.Find("EnemyObject");
        UFO = GameObject.Find("UFOObject");
        //シーン遷移のオブジェクトを取得する
        scene = GameObject.Find("SceneObject");
    }

    // スコアを加算する
    void AddScore(int score)
    {
        Score.score += score;
    }

    void Update()
    {
        // Text(得点)を書き換える
        GetComponent<Text>().text = "Score:" + Score.score;
        //得点が一定数まで達したら
        if(score >= 10000)
        {
            //敵を削除して
            Destroy(Enemy.gameObject);
            Destroy(UFO.gameObject);
            //ボールオブジェクトを削除して
            Destroy(Ball.gameObject);
            //その後、ゲームクリア画面に切り替える
            GameClearObject.SetActive(true);
            //3秒待ってからシーン遷移に移る
            Invoke("scenechange",3);
        }
    }
    void scenechange()
    {
        //ボスステージに移行
        scene.SendMessage("BossScene");
    }

}