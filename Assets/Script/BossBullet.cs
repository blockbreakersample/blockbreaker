﻿using UnityEngine;
using System.Collections;
/// <summary>
/// ボスの一つ目の攻撃のスクリプト
/// </summary>
public class BossBullet : MonoBehaviour {

    //弾が向かってくる速度
    private float speed = 5f;
    //ターゲット(プレイヤー)がどのベクトル(x軸、y軸)にいるかを格納
    private Vector2 target;
    //プレイヤーを格納する変数
    private GameObject player;
    //初期化処理
    public void Start()
    {
        //Playerオブジェクトを全て取得
        player = GameObject.Find("Player");
        //Playerオブジェクトの位置情報をtarget変数に格納
        target = player.transform.position;

        //弾が生成される時、カメラの最も右端である0.0のx軸の情報を参照し、
        //右端の0から、プレイヤーの位置がどの程度左にいる(x軸の座標の値が低い)のかを取得し、
        if (target.x < this.transform.position.x)
        {
            //そのx軸の値を代入する
            transform.position = new Vector3(-1, transform.position.y, transform.position.z);
        }

    }

    private void Update()
    {
        //弾をターゲット(プレイヤー)と同じx軸の位置まで動かす
        transform.position = Vector2.MoveTowards(transform.position, new Vector2(target.x, transform.position.y), speed * Time.deltaTime);
        //プレイヤーと同じ位置まで動かしたら、今度は弾を下に動かしていく
        //パソコンの処理速度がコードに影響しないようにTime.deltaTimeで処理速度を一定にする。
        transform.position += new Vector3(0, -speed * Time.deltaTime, 0);
    }
    //Colliderが他のトリガーイベントに侵入したときに呼び出される関数(物理的な干渉はしない)
    protected void OnTriggerEnter(Collider other)
    {
        //bottomwallのタグのついたトリガーに触った時
        if (other.gameObject.tag == "bottomwall")
        {
            //弾を削除
            Destroy(this.gameObject);
        }
        //Blockのタグのついたトリガーに触った時
        if (other.gameObject.tag == "Block")
        {
            //ブロックを削除
            Destroy(this.gameObject);
        }
    }
}
