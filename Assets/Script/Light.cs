﻿using UnityEngine;
using System.Collections;

/// <summary>
/// 全てのシーンでライトが存在するようにするスクリプト
/// </summary>
public class Light : MonoBehaviour {

    //初期化処理
    void Awake()
    {
        //ライトオブジェクトがシーン遷移しても破棄されないようにする
        DontDestroyOnLoad(this);
    }
}
