﻿using UnityEngine;
using System.Collections;

/// <summary>
/// 2体目(UFO)の敵に関する行動を規定するスクリプトコード(EnemyDefaultスクリプトを継承する)
/// </summary>
public class Enemy : EnemyDefault
{

    //敵の発射する弾のPrefabを格納する変数
    [SerializeField]
    private GameObject EnemyBullet;

    //爆発オブジェクトを格納するための変数の宣言
    [SerializeField]
    private GameObject explosion;

    //倒された時に加算される得点
    public int score = 500;

    // 待ち時間(ランダムに変更する).
    private float waitTime;
    // 待ち時間計測.
    private float waitTimer = 0.0f;
    //ランダム数値の最低値
    private float range_x = 10.0f;
    //ランダム数値の最大値
    private float range_y = 50.0f;

    // Playerを定義する変数
    GameObject player;

    void Awake()
    {
        // UFOが1体で見る場合、タイマーは動かさず、2体目以降のUFOは削除する
        if (GameObject.FindGameObjectsWithTag("UFO").Length > 1)
        {
            // タイマーは0のままにする
            waitTimer = 0.0f;
            //2つ目以降のUFOは、出てきた場合破棄する
            Destroy(gameObject);
            return;
        }
    }
    //EnemyDefaultのStart関数をオーバーライドで上書きする
    protected override void Start()
    {
        //待ち時間を初期化(これで一斉射撃しなくなる)
        waitTime = Random.Range(range_x, range_y);
    }

    //スクリプトが動ける限り何回でも動くコード
    //EnemyDefaultのUpdate()を上書きする
    protected override void Update()
    {
        //上書きによって、子オブジェクトを探すif文を取り除いている。
        //Move関数でX軸方向に動かす(最初は左方向)
        Move(transform.right * directionX);

        // 時間を測る(時間を加算していく)
        waitTimer += Time.deltaTime;
        // 待ち時間が経過したら
        if (waitTimer > waitTime)
        {
            // 次の待ち時間をランダムに設定，1~20秒の範囲.
            waitTime = Random.Range(range_x, range_y);
            // タイマーは0に戻す
            waitTimer = 0.0f;

            //敵の位置に球を生成
            Instantiate(EnemyBullet, new Vector2(transform.position.x,
                transform.position.y), Quaternion.identity);
        }

    }
    /// <summary>Rigidbody(物理演算)を持つオブジェクトとColliderを持つオブジェクトが衝突した時にこの関数が呼ばれる</summary>
    /// <param name="collision"></param>
    void OnCollisionEnter(Collision collision)
    {
        //sidewallのタグが付いたオブジェクトのcolliderとぶつかった時
        if (collision.gameObject.tag == "sidewall")
        {
            //X軸に動く方向を反転させる
            directionX = directionX * -1;
        }
        //衝突したオブジェクトのtagがBallならば
        if (collision.gameObject.tag == "Ball")
        {
            //敵の位置に爆発オブジェクトを生成する
            Instantiate(explosion, transform.position, transform.rotation);
            //スコアオブジェクトに設定したスコア数を足していく
            GameObject.Find("Score").SendMessage("AddScore", score);
            //敵オブジェクトを消滅させる
            Destroy(this.gameObject);
        }
    }
}
