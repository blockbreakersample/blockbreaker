﻿using UnityEngine;
using System.Collections;
/// <summary>
/// ボスの動きに関するスクリプト
/// </summary>
public class Boss : EnemyDefault {


    //敵の発射する弾(拡散弾)のPrefabを格納する変数
    [SerializeField]
    private GameObject DiffsionBullet;

    //もう一つ発射する弾(自機狙い弾)を格納
    [SerializeField]
    private GameObject BossBullet;

    //爆発オブジェクトを格納するための変数の宣言
    [SerializeField]
    private GameObject explosion;
    //ボールオブジェクトを格納する変数を宣言
    private GameObject Ball;
    //ゲームクリア時にテキストを表示させるオブジェクトを格納する変数の宣言
    [SerializeField]
    private GameObject GameClearObject;
    //シーン遷移をするためのシーンオブジェクトを格納する変数の宣言
    [SerializeField]
    private GameObject scene;

    //ボスの体力を格納する変数の宣言
    private int Life;

    // 待ち時間(ランダムに変更する).
    private float waitTime;

    // 待ち時間計測.
    private float waitTimer = 0.0f;
    //拡散弾のランダム数値の最低値
    private float range_x = 3.0f;
    //拡散弾のランダム数値の最大値
    private float range_y = 15.0f;
    //自機狙い弾のランダム数値の最低値
    private float range_x2 = 0.5f;
    //自機狙い弾のランダム数値の最大値
    private float range_y2 = 2.5f;

    // Use this for initialization
    protected override void Start()
    {
        //ボールのオブジェクトを取得する
        Ball = GameObject.Find("Ball");
        //敵のオブジェクトを取得する
        //シーン遷移のオブジェクトを取得する
        scene = GameObject.Find("SceneObject");
        //体力を30に設定
        Life = 30;
    }

    protected override void Update()
    {
        //上書きによって、子オブジェクトを探すif文を取り除いている。
        //ボスの体力が20以下になったら
        if (Life <= 20)
        {
            //Move関数でX軸方向に動かす(最初は左方向)
            Move(transform.right * directionX);

            // 時間を測る(時間を加算していく)
            waitTimer += Time.deltaTime;
            // 待ち時間が経過したら
            if (waitTimer > waitTime)
            {
                // 次の待ち時間をランダムに設定
                waitTime = Random.Range(range_x, range_y);
                // タイマーは0に戻す
                waitTimer = 0.0f;

                //敵の位置に拡散弾を生成
                Instantiate(DiffsionBullet, new Vector2(transform.position.x,
                    transform.position.y), Quaternion.identity);
            }
        }
        //それ以外(体力が20を下回っていないのなら横方向に動かない)
        else
        {
            // 時間を測る(時間を加算していく)
            waitTimer += Time.deltaTime;
            // 待ち時間が経過したら
            if (waitTimer > waitTime)
            {
                // 次の待ち時間をランダムに設定
                waitTime = Random.Range(range_x2, range_y2);
                // タイマーは0に戻す
                waitTimer = 0.0f;

                //敵の位置に自機狙い弾を生成
                Instantiate(BossBullet, new Vector2(transform.position.x,
                    transform.position.y), Quaternion.identity);
            }
        }
    }

    protected void OnCollisionEnter(Collision collision)
    {
        //sidewallのタグが付いたオブジェクトのcolliderとぶつかった時
        if (collision.gameObject.tag == "sidewall")
        {
            //X軸に動く方向を反転させる
            directionX = directionX * -1;
        }
        //衝突したオブジェクトのtagがBallならば
        if (collision.gameObject.tag == "Ball")
        {
            //体力を1下げる
            Life--;
            //体力が0になったら
            if (Life == 0)
            {
                //敵の位置に爆発オブジェクトを生成する
                Instantiate(explosion, transform.position, transform.rotation);
                //ボールオブジェクトを削除して
                Destroy(Ball.gameObject);
                //その後、ゲームクリア画面に切り替える
                GameClearObject.SetActive(true);
                //敵オブジェクトを消滅させる
                GetComponent<Renderer>().enabled = false;
                //3秒待ってからシーン遷移に移る
                Invoke("scenechange", 3);
            }
        }
    }
    void scenechange()
    {
        //タイトルに戻す
        scene.SendMessage("ReturnScene");
    }
}

