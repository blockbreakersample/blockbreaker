﻿using UnityEngine;
using System.Collections;

using System.Collections.Generic;
using UnityEngine.SceneManagement;

/// <summary>
/// 敵の生成を管理するクラス(EnemyDefaultクラスを継承している)
/// </summary>
public class Enemyctrl : EnemyDefault
{

    //敵のPrefabを格納できる場所を設定
    public GameObject EnemyPrefab;

    // 下(Y軸)に移動する距離
    private float downY = 0.25f;

    //敵の幅の変数
    public float Enemysize = 0.27f;

    // EnemyObject(親オブジェクト)の中心点が左に移動できる範囲を定義する変数
    Vector2 leftEnemyObjectMax;
    // EnemyObject(親オブジェクト)の中心点が右に移動できる範囲を定義する変数
    Vector2 rightEnemyObjectMax;
    // EnemyObject(親オブジェクト)の中心点が下に移動できる範囲を定義する変数
    Vector2 downEnemyObjectMax;

    // X軸(横方向)の生成する数(Inspecterで指定する)
    public int createNumbers_X;
    // Y軸(縦方向)の生成する数(Inspecterで指定する)
    public int createNumbers_Y;
    // 敵を生成する間隔(Inspecterで指定する)
    public int space;
    //listのジェネリックコレクションの宣言,初期化
    List<GameObject> list = new List<GameObject>();

    //このスクリプトが動くときに一回だけ処理されるプログラム
    //EnemyDefaultのstart()を上書きする
    protected override void Start()
    {
        base.Start();

        //敵の変化する値を初期値に戻す
        speed = 0f;
        EnemyShot.score = 0;
        EnemyShot.range_y = 50.0f;

        // 生成メソッド実行
        CreateObject();
    }

    // 生成メソッド
    public void CreateObject()
    {
        //敵が生成されるたびに少しずつ移動速度を上げる
        speed += 0.25f;
        //敵が生成されるたびに倒したときの得点を上げる
        EnemyShot.score += 50;
        //敵が生成されるたびに攻撃頻度を上げる
        EnemyShot.range_y -= 5.0f;
        //敵が生成されるたびにボールの速度を上げていく
        Ball.ball_x += 0.5f;
        Ball.ball_y += 0.5f;

        //リストを消去する
        list.Clear();
        //int型のyが設定した生成する数よりも小さい場合、ｙを１ずつ加算していき、敵オブジェクトを縦に並べていく。
        for (int y = 0; y < createNumbers_Y; y++)
        {
            //int型のyが加算される場合、同様にxも1ずつ加算していき、加算されたy軸の場所から敵オブジェクトを横に並べていく。
            for (int x = 0; x < createNumbers_X; x++)
            {
                //リストの中に生成されるEnemyオブジェクトを入れ込む
                list.Add((GameObject)Instantiate(EnemyPrefab, createPosition + new Vector2(space * x, space * y), Quaternion.identity));
            }

        }

        //foreachでGameObject(この場合,配列に置かれた敵)を一つずつ処理していく
        foreach (GameObject child in list)
        {
            //親オブジェクトが子オブジェクトを取得し、それぞれの子オブジェクトを指定された場所に入れる。
            child.transform.SetParent(this.gameObject.transform, false);
        }
    }
    // Update is called once per frame
    //スクリプトが動いている限り何回でも処理されるプログラム
    //EnemyDefaultのUpdate()を上書きする
    protected override void Update()
    {

        //子オブジェクト(この場合Enemyオブジェクトのこと)が一つでも残っていれば
        if (transform.childCount > 0)
        {
            //下記のコードにある敵キャラの中で一番右端と一番左端と一番下の位置を格納して
            GetEnemyMaxPosition();
            //Move関数でX軸方向に動かす(最初は左方向)
            Move(transform.right * directionX);
        }

        //子オブジェクト(Enemyオブジェクト)が一つも残っていなかったら
        if (transform.childCount == 0)
        {
            this.transform.position = new Vector3(0, 0, 0);
            // 生成メソッド実行
            CreateObject();
        }
    }
    //プチノイズを防ぐ為 音量をコルーチンでフェードアウトさせる 
    IEnumerator FadeOutTime(float duration)
    {
        //タイムに初期値の設定
        float time = 0.0f;
        //停止時間を設定
        float wait = 0.2f;
        //コンポーネントのボリューム音量を拾う
        float firstVolume = GetComponent<AudioSource>().volume;
        //duration(止めておく時間)がtimeに設定した数よりも大きくなるまで
        while (duration > time)
        {
            //firstVolume～0へフェードアウトしていく
            GetComponent<AudioSource>().volume = Mathf.Lerp(firstVolume, 0, time / duration);
            //waitで指定された時間まで待機し
            yield return new WaitForSeconds(wait);
            //time関数にwait関数を足す
            time += wait;
        }
        //一連の動作が終わったら音を削除する
        DestroyObject(gameObject);
    }
    /// <summary>
    /// x軸・Y軸の方向にオブジェクト(敵)を動かすためのコード
    /// </summary>
    /// <param name="direction"></param>
    /// EnemyDefaultのMoveを上書きする。
    protected override void Move(Vector2 direction)
    {

        //敵の現在の座標を取得
        Vector2 position = transform.position;

        // 移動量を加える(Time.deltaTimeで速度を一定にする)
        position += direction * speed * Time.deltaTime;

        // 敵(親オブジェクトの中心点)が横方向に動ける最大値を制限（暴走して下に突っ込む可能性の対策
        //leftEnemyObjectMax.xが左方向、rightEnemyObjectMax.xが右方向の最大値となる
        position.x = Mathf.Clamp(position.x, leftEnemyObjectMax.x, rightEnemyObjectMax.x);

        // 左の端か右の端に到達している時は
        if (position.x <= leftEnemyObjectMax.x || position.x >= rightEnemyObjectMax.x)
        {
            //X軸に動く方向を反転させる
            directionX = directionX * -1;
            //少し下へ移動させる
            position.y = position.y - downY;
            //Enemyctrlオブジェクトについた移動時の効果音が鳴る
            GetComponent<AudioSource>().Play();
        }

        // 制限をかけた値を新しい位置とする
        transform.position = position;
    }
    // 敵キャラの中で一番右端と一番左端と一番下の位置を格納する
    public void GetEnemyMaxPosition()
    {
        //敵の位置を定義する変数
        Transform enemytransform;

        // 画面左下のワールド座標をビューポートから取得
        Vector2 min = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));

        // 画面右上のワールド座標をビューポートから取得
        Vector2 max = Camera.main.ViewportToWorldPoint(new Vector2(1, 1));
        //これによって画面の描画範囲が敵の動ける最大範囲となる

        //最初にオブジェクト(敵)が絶対に到達しない右の最大のPosition情報を設定する
        Vector2 leftEnemyMax = new Vector2(999f, 0f);
        //同様に敵が到達しない左の最大のPosition情報を設定する
        Vector2 rightEnemyMax = new Vector2(-999f, 0f);
        // 敵が到達しない上の最大のPosition情報を設定する
        Vector2 downEnemyMax = new Vector2(0f, 999f);

        // 全ての敵キャラの位置を調べる
        for (int i = 0; i < transform.childCount; i++)
        {
            //Enemyの情報を1体ずつ生成された順に取得している
            enemytransform = transform.GetChild(i);

            // 生成されたEnemyと現在の右の最大のPosition情報を参照し、
            //Position情報よりもEnemyの位置が左である(x軸の座標の値が低い)のならば
            if (enemytransform.position.x < leftEnemyMax.x)
            {
                //そのEnemyのPosition情報に書き換える(これを一体ずつ繰り返していく)
                //これによって、その書き換えられたEnemyの位置が現在最も左端の敵ということとなる。
                leftEnemyMax = enemytransform.position;
            }
            // 生成されたEnemyと現在の左の最大のPosition情報を参照し、
            //Position情報よりもEnemyの位置が右である(x軸の座標の値が高い)のならば
            if (enemytransform.position.x > rightEnemyMax.x)
            {
                //そのEnemyのPosition情報に書き換える(これを一体ずつ繰り返していく)
                //これによって、その書き換えられたEnemyの位置が現在最も右端の敵ということとなる。
                rightEnemyMax = enemytransform.position;
            }
            // 生成されたEnemyと現在の上方向の最大のPosition情報を参照し、
            //Position情報よりもEnemyの位置が下である(y軸の座標の値が低い)のならば
            if (enemytransform.position.y < downEnemyMax.y)
            {
                //そのEnemyのPosition情報に書き換える(これを一体ずつ繰り返していく)
                downEnemyMax = enemytransform.position;
            }

        }
        //これによって、その書き換えられたEnemyの位置が現在最も下端の敵ということとなる。

        //EnemyObject(敵オブジェクトの親)の中心点がそれぞれの方向にどのくらい移動できるかを計算
        //計算したら上記のMathf.Clampでその範囲内に移動を制限する
        //値が変化したらその都度計算しなおす

        //敵の親オブジェクトの中心点が移動できるX軸の最低値(左の最大値)
        leftEnemyObjectMax.x = min.x - leftEnemyMax.x + Enemysize + transform.position.x;
        //-0.55 =  -2.8 - -2.5~-1.5 + 0.27 + 0

        //敵の親オブジェクトの中心点が移動できるX軸の最大値(右の最大値)
        rightEnemyObjectMax.x = max.x - rightEnemyMax.x - Enemysize + transform.position.x;
        //敵の親オブジェクトの中心点が移動できるy軸の最低値(下の最大値)
        downEnemyObjectMax.y = transform.position.y - downEnemyMax.y + Enemysize + transform.position.y;

        //min.x,max.xはそれぞれカメラに描画されている範囲の最低値と最大値。(左が最低値で右が最大値)
        //EnemyMaxは上のfor文で、今現在どの位置の敵がそれぞれの端の位置なのかを設定している
        //EnemySizeは敵の幅のことで、その値は0.27fである。
        //transform.positionは現在の親オブジェクトの位置である(初期値は(0,0)とする)。
        //これらの情報を元に現在の端にいる敵がカメラの描画範囲に収まるギリギリの位置の座標まで親オブジェクトを動かす
        //敵がカメラの範囲を超えそうになったら移動する向きを反対に変えて繰り返す。
        //端の敵が全て消えた場合には新たに端の敵の位置を計算しなおして親を動かす座標を更新する。

        //カメラのx軸の描画範囲(今回は0.58)をそのまま使うと、オブジェクトは自身の中心点を基準とするため引っ掛かりが生じる。
        //なので、端にいる敵の位置、生成された敵の幅、親の位置(初期値：０)の値を足していく(今回は-0.03)。
        //それを描画範囲の最大値に足すことでわずか内側にずらす(x軸は‐の値なので-で計算する)ことでカメラの範囲に収まるようにしている。
    }

}
