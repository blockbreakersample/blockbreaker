﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
/// <summary>
/// シーン遷移を行うためのクラス
/// </summary>
public class SceneChange : MonoBehaviour
{

    void Start()
    {

    }

    //タイトル画面からゲーム画面へ遷移
    //それぞれを対応しているボタンに付ける
    public void SceneLoad()
    {
        //ゲーム画面へ遷移するもの
        SceneManager.LoadScene("Main");
    }
    public void BossScene()
    {
        //ボスステージへ遷移するもの
        SceneManager.LoadScene("Boss");
    }
    //ゲーム終了のメソッド
    public void FinishScene()
    {
        //ゲームを終了させるもの
        Application.Quit();
    }
    //タイトル画面に戻るもの
    public void ReturnScene()
    {
        //タイトル画面へ遷移するもの
        SceneManager.LoadScene("Title");
    }
}