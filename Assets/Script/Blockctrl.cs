﻿using UnityEngine;
using System.Collections;

public class Blockctrl : MonoBehaviour
{
    //防御壁を設定するPrefabファイルを設定(Inspecter上で定義)
    public Transform BlockWallPrefab;

    // 生成する数
    public int createNumbers;
    // 生成する間隔
    public float space;
    // 生成する場所
    public Vector2 createPosition;

    // Use this for initialization
    void Start()
    {
        // 生成メソッド実行
        CreateObject();
    }

    // 生成メソッド
    void CreateObject()
    {
        //int型のyが加算される場合、同様にxも1ずつ加算していき、加算されたy軸の場所からブロックを横に並べていく。
        for (int x = 0; x < createNumbers; x++)
        {
            Instantiate(BlockWallPrefab, createPosition + new Vector2(space * x, 0), Quaternion.identity);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}