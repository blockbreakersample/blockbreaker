﻿using UnityEngine;
using System.Collections;
/// <summary>
/// 爆発のオブジェクトに関するスクリプト
/// </summary>
public class Explosion : MonoBehaviour {
    //爆発が終わったら
    public void DeleteExplosion()
    {
        //爆発オブジェクトを消去する
        Destroy(gameObject);
    }
}