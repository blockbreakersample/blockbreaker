﻿using UnityEngine;
using System.Collections;
using UnityEngine.SocialPlatforms.Impl;

/// <summary>
/// 敵の攻撃の速度と弾の速度に関するスプリクト
/// </summary>
public class EnemyShot : MonoBehaviour {
    //敵の弾をInspecterから関連付けるためにpublicによって用意した
    //プレハブのEnemyBulletとこのスプリクトを紐づける
    [SerializeField]
    private GameObject EnemyBullet;

    // 待ち時間(ランダムに変更する).
    private float waitTime;
    // 待ち時間計測.
    private float waitTimer = 0.0f;
    //ランダム数値の最低値
    private float range_x = 5.0f;
    //ランダム数値の最大値
    public static float range_y = 50.0f;

    //爆発オブジェクトを格納するための変数の宣言
    [SerializeField]
    private GameObject explosion;
    //倒された時の得点を格納する変数の宣言
    public static int score = 0;

    // Playerを定義する変数
    GameObject player;


    // Use this for initialization
    //このスプリクトが呼び出されたとき一回だけ実行される
    void Start () {
        //待ち時間を初期化(これで一斉射撃しなくなる)
        waitTime = Random.Range(range_x, range_y);

    }	
	// Update is called once per frame
	void Update () {

        // 時間を測る(時間を加算していく)
        waitTimer += Time.deltaTime;
        // 待ち時間が経過したら
        if (waitTimer > waitTime){ 
            // 次の待ち時間をランダムに設定，1~20秒の範囲.
            waitTime = Random.Range(range_x, range_y);
            // タイマーは0に戻す
            waitTimer = 0.0f; 

            //敵の位置に球を生成
            Instantiate(EnemyBullet, new Vector2(transform.position.x, 
                transform.position.y), Quaternion.identity);
        }
    }
    /// <summary>Rigidbody(物理演算)を持つオブジェクトとColliderを持つオブジェクトが衝突した時にこの関数が呼ばれる</summary>
    /// <param name="collider"></param>
    void OnCollisionEnter(Collision collision)
    {
        //衝突したオブジェクトのtagがBallならば
        if (collision.gameObject.tag == "Ball")
        {
            //敵の位置に爆発オブジェクトを生成する
            Instantiate(explosion, transform.position, transform.rotation);
            //スコアオブジェクトに設定したスコア数を足していく
            GameObject.Find("Score").SendMessage("AddScore", score);
            //敵オブジェクトを消滅させる
            Destroy(this.gameObject);
        }
    }
}
